function init(){
    // Создание карты.

    // if($('#map-1').length > 0) {
    // 	var myMap_1 = new ymaps.Map("map-1", {
    // 	    center: [43.45996104527249,39.95140660360716],
    // 	    zoom: 13,
    // 	    controls: [],
    // 	}, {
	//
    // 		yandexMapDisablePoiInteractivity: true
    // 	});
	//
    // 	myMap_1.behaviors.disable('scrollZoom');
    // }

    if($('#map-2').length > 0) {
    	var myMap_2 = new ymaps.Map("map-2", {
    	    center: [43.483314, 39.894758],
    	    zoom: 16,
            controls: ['default']
    	}, {

    		yandexMapDisablePoiInteractivity: true
    	});

    	myMap_2.behaviors.disable('scrollZoom');

        // Создаем геообъект с типом геометрии "Точка".
        var myGeoObject = new ymaps.GeoObject({
            // Описание геометрии.
            geometry: {
                type: "Point",
                coordinates: [43.483314, 39.894758]
            },
            // Свойства.
            properties: {
                // iconColor: 'red',
                // preset: 'islands#redIcon'
            }
        });
        myMap_2.geoObjects.add(myGeoObject);

    }

}


$(document).ready(function () {
    if ($(window).width() > 1006) {

        $('.parallax-wrap').parallax({
            calibrateX: true,
            calibrateY: false,
            limitX: false,
            limitY: false,
            scalarX: 2,
            scalarY: 10,
            frictionX: 0.2,
            frictionY: 0.2
        });

    }


    $('.slider-big').slick({
        centerMode: true,
        centerPadding: '700px',
        slidesToShow: 1,
        infinite: false,
        dots: true,
        arrows: false,
        customPaging : function(slider, i) {
            var title = $(slider.$slides[i]).find('.slider-item-title').html();
            return '<span class="slider-big__pag-item"> '+ title +' </span>';
        },
        responsive: [
            {
                breakpoint: 3500,
                settings: {
                    slidesToShow: 1,
                    centerPadding: '500px',
                }
            },
            {
                breakpoint: 3200,
                settings: {
                    centerPadding: '300px',
                }
            },
            {
                breakpoint: 2850,
                settings: {
                    centerPadding: '0px'
                }
            },
            // {
            //   breakpoint: 3200,
            //   settings: {
            //     centerPadding: '500px',
            //   }
            // },
            // {
            //   breakpoint: 1240,
            //   settings: {
            //     slidesToShow: 1,
            //     centerPadding: '145px',
            //   }
            // },
            // {
            //   breakpoint: 1140,
            //   settings: {
            //     slidesToShow: 1,
            //     centerPadding: '45px',
            //   }
            // },
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 1,
            //     centerMode: false,
            //   }
            // },
        ]
    });


    $(".territory__slider-btn--prev").on("click", function() {
        $(".slider-big").slick("slickPrev");
    });

    $(".territory__slider-btn--next").on("click", function() {
        $(".slider-big").slick("slickNext");
    });



    $('.build-slider').slick({
        slidesToShow: 4,
        infinite: true,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 3,

                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,

                }
            },

            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '110px',
                    infinite: false,

                }
            },

            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '90px',
                    infinite: false,

                }
            },

            {
                breakpoint: 410,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '70px',
                    infinite: false,

                }
            },

            {
                breakpoint: 360,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '50px',
                    infinite: false,

                }
            },

        ]

    });

    $('.build-slider__slider-btn--prev').on('click', function () {
        var $this = $(this),
            $slider = $this.closest('.build-slider__wrapper').find('.build-slider');
        $slider.slick('slickPrev');
    });
    $('.build-slider__slider-btn--next').on('click', function () {
        var $this = $(this),
            $slider = $this.closest('.build-slider__wrapper').find('.build-slider');
        $slider.slick('slickNext');
    });

    // $(".sl-btn-1-p").on("click", function() {
    //     $("#slider-1").slick("slickPrev");
    // });
    // $(".sl-btn-1-n").on("click", function() {
    //     $("#slider-1").slick("slickNext");
    // });
    // $(".sl-btn-2-p").on("click", function() {
    //     $("#slider-2").slick("slickPrev");
    // });
    // $(".sl-btn-2-n").on("click", function() {
    //     $("#slider-2").slick("slickNext");
    // });
    // $(".sl-btn-3-p").on("click", function() {
    //     $("#slider-3").slick("slickPrev");
    // });
    // $(".sl-btn-3-n").on("click", function() {
    //    $("#slider-3").slick("slickNext");
    // });



    $('.burger-content').on('click', function () {
        $('.header__menu').addClass('active');
        $('body').addClass('overflowHidden');
    });

    $('.mobile-menu-close').on('click', function () {
        $('.header__menu').removeClass('active');
        $('body').removeClass('overflowHidden');
    });


    $('.name-tel-form').each(function(i, item) {
        var $form = $(item);

        $form.validate({
            rules: {
                tel: {
                    required: true,
                },
                name: {
                    required: true
                }

            },
            messages: {
                tel: {
                    required: 'Укажите ваш телефон'
                },
                name: {
                    required: 'Укажите ваше имя'
                }
            },
            submitHandler: function(form) {
                var $form = $(form);
                $.ajax({
                    type: "POST",
                    url: "name-tel-field.php",
                    data: $form.serialize()
                }).done(function(e) {
                    /* берем из формы id цели яндекс метрики */
                    // var $ym_input = $form.find('input[name="ym_id"]');
                    // if ($ym_input.length > 0) {
                    //     var ym_id = $ym_input.val();
                    /* отправляем цель в я.метрику */
                    // YMGoal(ym_id);
                    // }
                    /* берем из формы id события GA */
                    // var $ga_input = $form.find('input[name="ga_id"]');
                    // if ($ga_input.length > 0) {
                    //     var ga_id = $ga_input.val();
                    /* отправляем в GA */
                    // GASendForm(ga_id);
                    // }

                    $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                    $form.trigger('reset');
                    window.location = "thanks-page.html";

                    // fbq('track', 'Lead_aleksandrit', {
                    //     value: 5,
                    // });
                }).fail(function (e) {
                    $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                    $form.trigger('reset');
                    setTimeout(function () {
                        $form.find('.dispatch-message').slideUp();
                    }, 5000);
                })
            }
        });

    });


    $('.tel-form').each(function(i, item) {
        var $form = $(item);

        $form.validate({
            rules: {
                tel: {
                    required: true,
                },

            },
            messages: {
                tel: {
                    required: 'Укажите ваш телефон'
                },
            },
            submitHandler: function(form) {
                var $form = $(form);
                $.ajax({
                    type: "POST",
                    url: "tel-field.php",
                    data: $form.serialize()
                }).done(function(e) {
                    /* берем из формы id цели яндекс метрики */
                    // var $ym_input = $form.find('input[name="ym_id"]');
                    // if ($ym_input.length > 0) {
                    //     var ym_id = $ym_input.val();
                    /* отправляем цель в я.метрику */
                    // YMGoal(ym_id);
                    // }
                    /* берем из формы id события GA */
                    // var $ga_input = $form.find('input[name="ga_id"]');
                    // if ($ga_input.length > 0) {
                    //     var ga_id = $ga_input.val();
                    /* отправляем в GA */
                    // GASendForm(ga_id);
                    // }

                    $form.find('.dispatch-message').removeClass('error').addClass('success').html('Спасибо! Мы свяжемся с вами').slideDown();
                    $form.trigger('reset');
                    window.location = "thanks-page.html";

                    // fbq('track', 'Lead_aleksandrit', {
                    //     value: 5,
                    // });
                }).fail(function (e) {
                    $form.find('.dispatch-message').removeClass('success').addClass('error').html('Произошла ошибка. Попробуйте еще раз').slideDown();
                    $form.trigger('reset');
                    setTimeout(function () {
                        $form.find('.dispatch-message').slideUp();
                    }, 5000);
                })
            }
        });

    });





    $(".tabs-js").on("click", "li:not(.active)", function(e) {
        $(this)
            .addClass("active")
            .siblings()
            .removeClass("active")
            .parents(".tabs-info-js")
            .find(".build-main__tabs")
            .children()
            .hide()
            .eq($(this).index())
            .fadeIn(300);

        var tab = $(this).attr('data-tab');

        var params = new URLSearchParams(location.search);
        if (params.has('tab')) {
            params.set('tab', tab);
        } else {
            params.append('tab', tab);
        }
        window.history.replaceState({}, '', `${location.pathname}?${params}`);

        e.preventDefault();

    });
    // if ($('.build-main__content.tabs-info-js').length > 0) {
    //     var url_string = window.location.href,
    //         url = new URL(url_string),
    //         tab = url.searchParams.get('tab');
    //     var $tabItems = $('.build-main__tab-item');
    //     if (tab === 'photos') {
    //         $('.tabs-js li[data-tab="photos"]').click();
    //         $tabItems.removeClass('hidden');
    //     } else if (tab === 'webcam') {
    //         $('.tabs-js li[data-tab="webcam"]').click();
    //         $tabItems.removeClass('hidden');
    //     } else {
    //         $('.tabs-js li[data-tab="photos"]').click();
    //         $tabItems.removeClass('hidden');
    //     }
    // }


    $('input[name="tel"]').inputmask({mask: "+7 (999) 9999999"});

    $("a.scrollTo").on('click', function () {
        var elementClick = $(this).attr("href"),
            destination = $(elementClick).offset().top;
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
        if ($(window).width() <= 1023) {
            if ($('.header__menu').hasClass('active')) {
                $('.mobile-menu-close').click();
            }
        }
        return false;
    });



    $('.lazy').Lazy();
});


if (typeof ymaps === 'object') {
    ymaps.ready(init);
}
